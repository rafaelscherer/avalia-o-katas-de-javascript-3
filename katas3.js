

function jump() {
    var node = document.createElement('br');

    var destination = document.getElementById("d1");
    destination.appendChild(node);
}


for (i = 1; i <= 3; i++) {
    // Create a div, with class "bar", and set the width to 100px.
    var newElement = document.createElement("div");
    newElement.className = "bar";
    newElement.style.width = i * 100 + "px";

    // Place a text label inside the new div.
    var newText = document.createTextNode("Bar #" + i);
    newElement.appendChild(newText);

    // Put the new div on the page inside the existing element "d1".
    var destination = document.getElementById("d1");
    destination.appendChild(newElement);
}

function kata1() {
    for (i = 1; i <= 25; i++) {
        if (i < 25) {
            resultado = i + ", ";
        }
        else {
            resultado = i + ".";
        }

        var node = document.createElement("span");

        var newText = document.createTextNode(resultado);
        node.appendChild(newText);

        var destination = document.getElementById("d1");
        destination.appendChild(node);
    }

}
kata1();
jump();


function kata2() {
    for (i = 25; i >= 1; i--) {
        if (i > 1) {
            resultado = i + ", ";
        }
        else {
            resultado = i + ".";
        }
        var node = document.createElement("span");

        var newText = document.createTextNode(resultado);
        node.appendChild(newText);

        var destination = document.getElementById("d1");
        destination.appendChild(node);
    }
}
kata2();
jump();

function kata3() {
    for (i = 1; i <= 25; i++) {
        if (i < 25) {
            resultado = -i + ", ";
        }
        else {
            resultado = -i + ".";
        }

        var node = document.createElement("span");

        var newText = document.createTextNode(resultado);
        node.appendChild(newText);

        var destination = document.getElementById("d1");
        destination.appendChild(node);
    }
}
kata3();
jump();

function kata4() {
    for (i = 25; i >= 1; i--) {
        if (i > 1) {
            resultado = -i + ", ";
        }
        else {
            resultado = -i + ".";
        }

        var node = document.createElement("span");

        var newText = document.createTextNode(resultado);
        node.appendChild(newText);

        var destination = document.getElementById("d1");
        destination.appendChild(node);
    }
}
kata4();
jump();

function kata5() {
    for (i = 25; i >= -25; i--) {
        if (i % 2 != 0) {

            if (i > -25) {
                resultado = i + ", ";
            }
            else {
                resultado = i + ".";
            }

            var node = document.createElement("span");

            var newText = document.createTextNode(resultado);
            node.appendChild(newText);

            var destination = document.getElementById("d1");
            destination.appendChild(node);
        }
    }
}
kata5();
jump();

function kata6() {
    for (i = 3; i <= 100; i++) {
        if (i % 3 == 0) {

            if (i < 99) {
                resultado = i + ", ";
            }
            else {
                resultado = i + ".";
            }

            var node = document.createElement("span");

            var newText = document.createTextNode(resultado);
            node.appendChild(newText);

            var destination = document.getElementById("d1");
            destination.appendChild(node);
        }
    }
}
kata6();
jump();

function kata7() {
    for (i = 7; i <= 100; i++) {
        if (i % 7 == 0) {

            if (i < 98) {
                resultado = i + ", ";
            }
            else {
                resultado = i + ".";
            }

            var node = document.createElement("span");

            var newText = document.createTextNode(resultado);
            node.appendChild(newText);

            var destination = document.getElementById("d1");
            destination.appendChild(node);
        }
    }
}
kata7();
jump();

function kata8() {
    for (i = 100; i >= 3; i--) {
        if ((i % 3 == 0) || (i % 7 == 0)) {

            if (i > 3) {
                resultado = i + ", ";
            }
            else {
                resultado = i + ".";
            }
            var node = document.createElement("span");

            var newText = document.createTextNode(resultado);
            node.appendChild(newText);

            var destination = document.getElementById("d1");
            destination.appendChild(node);

        }
    }

}
kata8();
jump();

function kata9() {
    for (i = 5; i <= 95; i++) {
        if ((i % 2 != 0) && (i % 5 == 0)) {

            if (i < 95) {
                resultado = i + ", ";
            }
            else {
                resultado = i + ".";
            }
            var node = document.createElement("span");

            var newText = document.createTextNode(resultado);
            node.appendChild(newText);

            var destination = document.getElementById("d1");
            destination.appendChild(node);
        }
    }
}
kata9();
jump();

const sampleArray = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];

function kata10() {
    var numeros = sampleArray;
    for (i = 0; i < numeros.length; i++) {
        resultado = numeros[i] + ", ";

        var node = document.createElement("span");

        var newText = document.createTextNode(resultado);
        node.appendChild(newText);

        var destination = document.getElementById("d1");
        destination.appendChild(node);
    }
}
kata10();
jump();

function kata11() {
    var numeros = sampleArray;
    for (i = 0; i < numeros.length; i++) {
        if (numeros[i] % 2 == 0) {
            resultado = numeros[i] + ", ";


            var node = document.createElement("span");

            var newText = document.createTextNode(resultado);
            node.appendChild(newText);

            var destination = document.getElementById("d1");
            destination.appendChild(node);
        }
    }
}
kata11();
jump();

function kata12() {
    var numeros = sampleArray;
    for (i = 0; i < numeros.length; i++) {
        if (numeros[i] % 2 != 0) {
            resultado = numeros[i] + ", ";


            var node = document.createElement("span");

            var newText = document.createTextNode(resultado);
            node.appendChild(newText);

            var destination = document.getElementById("d1");
            destination.appendChild(node);
        }
    }
}
kata12();
jump();

function kata13() {
    var numeros = sampleArray;
    for (i = 0; i < numeros.length; i++) {
        if (numeros[i] % 8 == 0) {
            resultado = numeros[i] + ", ";


            var node = document.createElement("span");

            var newText = document.createTextNode(resultado);
            node.appendChild(newText);

            var destination = document.getElementById("d1");
            destination.appendChild(node);
        }
    }
}
kata13();
jump();

function kata14() {
    var numeros = sampleArray;
    for (i = 0; i < numeros.length; i++) {
        resultado = (numeros[i] * numeros[i]) + ", ";

        var node = document.createElement("span");

        var newText = document.createTextNode(resultado);
        node.appendChild(newText);

        var destination = document.getElementById("d1");
        destination.appendChild(node);
    }
}
kata14();
jump();

function kata15() {

    var resultado = 0;
    for (i = 1; i <= 20; i++) {
        resultado = resultado + i;
    }
    var node = document.createElement("span");

    var newText = document.createTextNode(resultado);
    node.appendChild(newText);

    var destination = document.getElementById("d1");
    destination.appendChild(node);

}
kata15();
jump();

function kata16() {

    var numeros = sampleArray;
    var resultado = 0;
    for (i = 0; i < numeros.length; i++) {
        resultado = resultado + numeros[i];

    }
    var node = document.createElement("span");

    var newText = document.createTextNode(resultado);
    node.appendChild(newText);

    var destination = document.getElementById("d1");
    destination.appendChild(node);

}
kata16();
jump();

function kata17() {

    var numeros = sampleArray;
    var aux = numeros[1];

    for (i = 0; i < numeros.length; i++) {

        if (aux > numeros[i]) {
            aux = numeros[i];
        }
        else {
            aux = aux;
        }
        resultado = aux;

    }
    var node = document.createElement("span");

    var newText = document.createTextNode(resultado);
    node.appendChild(newText);

    var destination = document.getElementById("d1");
    destination.appendChild(node);


}
kata17();
jump();


function kata18() {

    var numeros = sampleArray;
    var aux = numeros[1];

    for (i = 0; i < numeros.length; i++) {

        if (aux < numeros[i]) {
            aux = numeros[i];
        }
        else {
            aux = aux;
        }
        resultado = aux;

    }
    var node = document.createElement("span");

    var newText = document.createTextNode(resultado);
    node.appendChild(newText);

    var destination = document.getElementById("d1");
    destination.appendChild(node);
    

}
kata18();
jump();



















